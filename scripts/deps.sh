#!/bin/bash

set -e

# Update package lists
apt-get update -yqq

# Install necessary utilities
apt-get install ca-certificates wget sudo curl gnupg2 lsb-release libmagic1 -yqq

# Create directory for the PostgreSQL GPG key
install -d /usr/share/postgresql-common/pgdg

# Import the PostgreSQL repository GPG key
curl -o /usr/share/postgresql-common/pgdg/apt.postgresql.org.asc --fail https://www.postgresql.org/media/keys/ACCC4CF8.asc

# Add the PostgreSQL repository to the sources list
sh -c 'echo "deb [signed-by=/usr/share/postgresql-common/pgdg/apt.postgresql.org.asc] https://apt.postgresql.org/pub/repos/apt $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list'

# Update package lists again including the new PGDG repository
apt-get update -yqq

# Install PostgreSQL client & other dev dependencies
apt-get install -y postgresql-client-15 build-essential libpq-dev

# Cleanup
rm -rf /var/lib/apt/lists/*
