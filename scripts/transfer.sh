#!/bin/bash

DIR="$(cd "$(dirname "$0")" && pwd)"

SUB=$1
FROM=$2
TO=$3

$DIR/mods.py --sub $1 --add $3 --level 0
$DIR/mods.py --sub $1 --creator $3
$DIR/mods.py --sub $1 --remove $2
