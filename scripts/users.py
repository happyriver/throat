#!/usr/bin/env python3
import __fix
import argparse
import sys

import json

from peewee import fn
from app.models import (
    User,
    Client,
    Grant,
    Message,
    SiteLog,
    SiteMetadata,
    Sub,
    SubFlair,
    SubLog,
    SubMetadata,
    SubPost,
    SubPostComment,
    SubPostCommentVote,
    SubPostMetadata,
    SubPostVote,
    SubSubscriber,
    Token,
    UserMetadata,
    UserSaved,
    UserUploads,
    UserIgnores,
    SubPostPollOption,
    SubPostPollVote,
    SubPostReport,
    APIToken,
    APITokenSettings,
)
from app import create_app
from app.views.auth import send_login_link_email
import os
from itsdangerous import URLSafeTimedSerializer

app = create_app()

parser = argparse.ArgumentParser(description="Manage Users.")
addremove = parser.add_mutually_exclusive_group(required=True)
addremove.add_argument("-l", "--list", action="store_true", help="List Users")
addremove.add_argument("-m", "--modify", action="store_true", help="Modify Users")
modify = addremove.add_argument_group("Modify user")
modify.add_argument("-u", "--user", required=True, help="User to modify")
modify.add_argument("-s", "--status", help="Status Number")
modify.add_argument(
    "-r", "--resend", action="store_true", help="Resend registration email"
)

args = parser.parse_args()

if args.list:
    users = User.select().dicts()
    user_metadata = UserMetadata.select().dicts()

    user_to_metadata = {}
    for um in user_metadata:
        if not um["uid"] in user_to_metadata:
            user_to_metadata[um["uid"]] = []
        user_to_metadata[um["uid"]].append({"key": um["key"], "value": um["value"]})

    for user in users:
        user["metadata"] = user_to_metadata.get(user["uid"], [])

    print("Users: ")
    for i in users:
        print("  ", i)
elif args.modify:
    try:
        user = User.get(fn.Lower(User.name) == args.user.lower())
    except User.DoesNotExist:
        print("Error: User does not exist")
        sys.exit(1)
    if args.status:
        user.status = args.status
        user.save()
    elif args.resend:
        key = os.environ.get("APP_SECRET_KEY")
        s = URLSafeTimedSerializer(key, salt="login")
        token = s.dumps({"uid": user.uid})
        print(token)
