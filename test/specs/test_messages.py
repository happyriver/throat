from bs4 import BeautifulSoup
from flask import url_for
import pytest

from app.models import User, UserMetadata, SiteMetadata

from test.utilities import (
    csrf_token,
    register_user,
    log_in_user,
    log_out_current_user,
    promote_user_to_admin,
)


def substrings_present(data, snippets, exclude=False):
    """Return True if all the strings in `snippets` are found in `data`.
    If `exclude` is True, instead return True if none of the strings
    in `snippets` are found.

    """
    results = (snip.encode("utf-8") in data for snip in snippets)
    if exclude:
        results = (not val for val in results)
    return all(results)


def test_send_and_receive_dm(client, user_info, user2_info):
    """Send and receive a direct message."""
    username = user_info["username"]
    register_user(client, user_info)
    register_user(client, user2_info)

    # User2 sends User a message.
    rv = client.get(url_for("user.view", user=username))
    assert rv.status == "200 OK"
    client.post(
        url_for("do.create_sendmsg"),
        data=dict(
            csrf_token=csrf_token(rv.data),
            to=username,
            subject="Testing",
            content="Test Content",
        ),
        follow_redirects=True,
    )

    # User2 sees the message in Sent Messages.
    rv = client.get(url_for("messages.view_messages_sent"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(rv.data, [username, "Testing", "Test Content"])

    log_out_current_user(client, verify=True)
    log_in_user(client, user_info, expect_success=True)

    # User has one new message.
    rv = client.get(url_for("home.index"), follow_redirects=True)
    assert rv.status == "200 OK"
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    link = soup.find(href=url_for("messages.inbox_sort"))
    assert link.get_text().strip() == "1"

    # User sees the message on the inbox page.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"]
    )

    # User marks the message as read.
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    tag = soup.find(lambda tag: tag.has_attr("data-mid"))
    mid = tag.attrs["data-mid"]

    rv = client.post(
        url_for("do.read_pm", mid=mid),
        data=dict(csrf_token=csrf_token(rv.data)),
    )

    # User returns to home page; notifications count now 0.
    rv = client.get(url_for("home.index"), follow_redirects=True)
    assert rv.status == "200 OK"
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    link = soup.find(href=url_for("messages.inbox_sort"))
    assert link.get_text().strip() == "0"


def create_users_for_block_tests(client):
    def make_user_info(name):
        return dict(username=name, email=f"{name}@example.com", password="Pass1234")

    def make_user(info):
        register_user(client, info)
        log_out_current_user(client, verify=True)

    users = {
        name: make_user_info(name)
        for name in ["admin", "allblocker", "blocker", "blockee"]
    }

    for user in users.values():
        make_user(user)
    return users


def set_up_blocking_for_block_tests(client, users):
    log_in_user(client, users["admin"], expect_success=True)
    promote_user_to_admin(client, users["admin"])
    log_out_current_user(client, verify=True)

    # allblocker blocks everyone
    allblocker_uid = User.get(User.name == "allblocker").uid
    UserMetadata.create(uid=allblocker_uid, key="block_dms", value="1")

    # blocker blocks blockee
    log_in_user(client, users["blocker"], expect_success=True)
    rv = client.get(url_for("user.view", user="blockee"))
    blockee_uid = User.get(User.name == "blockee").uid
    rv = client.post(
        url_for("do.ignore_user", uid=blockee_uid),
        data=dict(csrf_token=csrf_token(rv.data)),
    )
    log_out_current_user(client, verify=True)


block_dms_expected_success = {
    "admin": {
        "admin": None,
        "allblocker": True,
        "blocker": True,
        "blockee": True,
    },
    "allblocker": {
        "admin": True,
        "allblocker": None,
        "blocker": False,
        "blockee": False,
    },
    "blocker": {
        "admin": True,
        "allblocker": False,
        "blocker": None,
        "blockee": False,
    },
    "blockee": {
        "admin": True,
        "allblocker": False,
        "blocker": False,
        "blockee": None,
    },
}


def test_block_dm_send(client):
    SiteMetadata.update(value="0").where(
        SiteMetadata.key == "site.send_pm_to_user_min_level"
    ).execute()

    users = create_users_for_block_tests(client)
    set_up_blocking_for_block_tests(client, users)

    for sender, sender_info in users.items():
        for receiver in users.keys():
            if sender == receiver:
                continue

            log_in_user(client, sender_info, expect_success=True)
            rv = client.get(url_for("user.view", user=receiver))
            assert rv.status == "200 OK"
            assert (b"/do/sendmsg" in rv.data) == block_dms_expected_success[sender][
                receiver
            ], f"sender={sender}, receiver={receiver}"

            rv = client.post(
                url_for("do.create_sendmsg"),
                data=dict(
                    csrf_token=csrf_token(rv.data),
                    to=receiver,
                    subject="Testing",
                    content="Test Content",
                ),
                follow_redirects=True,
            )
            assert (b"error" not in rv.data) == block_dms_expected_success[sender][
                receiver
            ], f"sender={sender}, receiver={receiver}"
            log_out_current_user(client, verify=True)


@pytest.mark.parametrize("sender", ["admin", "allblocker", "blocker", "blockee"])
def test_block_dm_reply(client, sender):
    SiteMetadata.update(value="0").where(
        SiteMetadata.key == "site.send_pm_to_user_min_level"
    ).execute()

    users = create_users_for_block_tests(client)

    # First make messages to reply to
    log_in_user(client, users[sender], expect_success=True)
    for receiver in users.keys():
        if sender == receiver:
            continue

        rv = client.get(url_for("user.view", user=receiver))
        assert rv.status == "200 OK"
        assert b"/do/sendmsg" in rv.data

        rv = client.post(
            url_for("do.create_sendmsg"),
            data=dict(
                csrf_token=csrf_token(rv.data),
                to=receiver,
                subject="Testing",
                content="Test Content",
            ),
            follow_redirects=True,
        )
        assert b"error" not in rv.data

    log_out_current_user(client, verify=True)
    set_up_blocking_for_block_tests(client, users)

    # Then go to the receiver and try to reply
    for receiver in users.keys():
        if sender == receiver:
            continue

        log_in_user(client, users[receiver], expect_success=True)
        # receiver sees the message on the inbox page.
        rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
        assert rv.status == "200 OK"
        assert substrings_present(
            rv.data, [users[sender]["username"], "Testing", "Test Content"]
        )

        # Attempt a reply
        soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
        tag = soup.find(lambda tag: tag.has_attr("data-mid"))
        mid = tag.attrs["data-mid"]

        rv = client.post(
            url_for("do.create_replymsg"),
            data=dict(
                csrf_token=csrf_token(rv.data), mid=mid, content="Test reply content"
            ),
        )
        assert (b"error" not in rv.data) == block_dms_expected_success[receiver][
            sender
        ], f"sender={sender}, receiver={receiver}"
        log_out_current_user(client, verify=True)


def test_save_and_delete_dm(client, user_info, user2_info):
    """Save and delete a direct message."""
    username = user_info["username"]
    register_user(client, user_info)
    register_user(client, user2_info)

    # User2 sends User a message.
    rv = client.get(url_for("user.view", user=username))
    assert rv.status == "200 OK"
    client.post(
        url_for("do.create_sendmsg"),
        data=dict(
            csrf_token=csrf_token(rv.data),
            to=username,
            subject="Testing",
            content="Test Content",
        ),
        follow_redirects=True,
    )

    # Switch users.
    log_out_current_user(client, verify=True)
    log_in_user(client, user_info, expect_success=True)

    # Message visible in User's inbox.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"]
    )

    # User saves the message.
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    tag = soup.find(lambda tag: tag.has_attr("data-mid"))
    mid = tag.attrs["data-mid"]
    rv = client.post(
        url_for("do.save_pm", mid=mid),
        data=dict(csrf_token=csrf_token(rv.data)),
        follow_redirects=True,
    )
    assert b"error" not in rv.data

    # Message now no longer appears in inbox.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"], exclude=True
    )

    # Message appears in saved messages.
    rv = client.get(url_for("messages.view_saved_messages"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"]
    )

    # User deletes message from saved messages.
    rv = client.post(
        url_for("do.delete_pm", mid=mid),
        data=dict(csrf_token=csrf_token(rv.data)),
        follow_redirects=True,
    )
    assert b"error" not in rv.data

    # Message is no longer in saved messages.
    rv = client.get(url_for("messages.view_saved_messages"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"], exclude=True
    )

    # Message is not in User's inbox either.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"], exclude=True
    )


def test_exchange_dm(client, user_info, user2_info):
    """Send a direct message and have an exchange of replies."""
    username = user_info["username"]
    register_user(client, user_info)
    register_user(client, user2_info)

    # User2 sends User a message.
    rv = client.get(url_for("user.view", user=username))
    assert rv.status == "200 OK"
    rv = client.post(
        url_for("do.create_sendmsg"),
        data=dict(
            csrf_token=csrf_token(rv.data),
            to=username,
            subject="Testing",
            content="Test Content",
        ),
        follow_redirects=True,
    )
    assert b"error" not in rv.data

    log_out_current_user(client, verify=True)
    log_in_user(client, user_info, expect_success=True)

    # User sees the message on the inbox page.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Testing", "Test Content"]
    )

    # User replies to the message.
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    tag = soup.find(lambda tag: tag.has_attr("data-mid"))
    mid = tag.attrs["data-mid"]

    rv = client.post(
        url_for("do.create_replymsg"),
        data=dict(
            csrf_token=csrf_token(rv.data), mid=mid, content="Test reply content"
        ),
    )
    assert b"error" not in rv.data

    # User sees the message in Sent Messages.
    rv = client.get(url_for("messages.view_messages_sent"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user2_info["username"], "Re: Testing", "Test reply content"]
    )

    log_out_current_user(client, verify=True)
    log_in_user(client, user2_info, expect_success=True)

    # User2 has one new message.
    rv = client.get(url_for("home.index"), follow_redirects=True)
    assert rv.status == "200 OK"
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    link = soup.find(href=url_for("messages.inbox_sort"))
    assert link.get_text().strip() == "1"

    # User2 sees the message on the inbox page.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user_info["username"], "Re: Testing", "Test reply content"]
    )

    # User2 replies to the reply.
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    tag = soup.find(lambda tag: tag.has_attr("data-mid"))
    mid = tag.attrs["data-mid"]
    rv = client.post(
        url_for("do.create_replymsg"),
        data=dict(
            csrf_token=csrf_token(rv.data), mid=mid, content="Test 2nd reply content"
        ),
    )
    assert b"error" not in rv.data

    # User2 sees reply in Sent Messages.
    rv = client.get(url_for("messages.view_messages_sent"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert substrings_present(
        rv.data, [user_info["username"], "Re: Testing", "Test 2nd reply content"]
    )
    assert b"Re: Re: Testing" not in rv.data

    log_out_current_user(client, verify=True)
    log_in_user(client, user_info, expect_success=True)

    # User now has two new messages.
    rv = client.get(url_for("home.index"), follow_redirects=True)
    assert rv.status == "200 OK"
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    link = soup.find(href=url_for("messages.inbox_sort"))
    assert link.get_text().strip() == "2"

    # User sees the message on the inbox page.
    rv = client.get(url_for("messages.inbox_sort"), follow_redirects=True)
    assert rv.status == "200 OK"
    assert b"Test 2nd reply content" in rv.data

    # User uses the mark all as read link.
    rv = client.post(url_for("do.readall_msgs"), data={})
    assert b"error" not in rv.data

    # User now has no new messages.
    rv = client.get(url_for("home.index"), follow_redirects=True)
    assert rv.status == "200 OK"
    soup = BeautifulSoup(rv.data, "html.parser", from_encoding="utf-8")
    link = soup.find(href=url_for("messages.inbox_sort"))
    assert link.get_text().strip() == "0"
