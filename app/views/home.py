""" Home, all and related endpoints """
import re
from feedgen.feed import FeedGenerator
from flask import (
    Blueprint,
    request,
    url_for,
    Response,
    render_template,
    redirect,
    send_from_directory,
    current_app,
)
from flask_login import current_user
from .. import misc
from ..misc import engine, limit_pagination
from ..misc import ratelimit, POSTING_LIMIT
from ..models import SubPost, Sub, dbp

bp = Blueprint("home", __name__)


@bp.route("/health")
def health():
    try:
        dbp.close_stale()
    except AttributeError:
        pass
    return send_from_directory(current_app.static_folder, "robots.txt")


@bp.route("/")
def index():
    """The index page, shows /hot of current subscriptions"""
    return hot(page=None)


@bp.route("/hot", defaults={"page": None})
@bp.route("/hot/<int:page>")
@limit_pagination
def hot(page):
    """/hot for subscriptions"""
    page_info = misc.getPostList(
        misc.postListQueryHome(sort="hot"),
        page=page,
        sort="hot",
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.hot",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {},
        }
    )


@bp.route("/new", defaults={"page": None})
@bp.route("/new/<int:page>")
@limit_pagination
def new(page):
    """/new for subscriptions"""
    page_info = misc.getPostList(
        misc.postListQueryHome(),
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.new",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {},
        }
    )


@bp.route("/top", defaults={"page": None})
@bp.route("/top/<int:page>")
@limit_pagination
def top(page):
    """/top for subscriptions"""
    page_info = misc.getPostList(
        misc.postListQueryHome(sort="top"),
        page=page,
        sort="top",
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.top",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {},
        }
    )


@bp.route("/all/new.rss")
def all_new_rss():
    """RSS feed for /all/new"""
    posts = misc.getPostList(misc.postListQueryBase())["posts"]
    fg = FeedGenerator()
    fg.id(request.url)
    fg.title("All new posts")
    fg.link(href=request.url_root, rel="alternate")
    fg.link(href=request.url, rel="self")

    return Response(
        misc.populate_feed(fg, posts).atom_str(pretty=True),
        mimetype="application/atom+xml",
    )


@bp.route("/apple-touch-icon-120x120.png")
@bp.route("/apple-touch-icon-120x120-precomposed.png")
@bp.route("/apple-touch-icon.png")
@bp.route("/apple-touch-icon-precomposed.png")
def static_img_from_root():
    return send_from_directory(current_app.static_folder, "/img" + request.path[1:])


@bp.route("/robots.txt")
@bp.route("/favicon.ico")
def static_from_root():
    return send_from_directory(current_app.static_folder, request.path[1:])


@bp.route("/all/new", defaults={"page": None})
@bp.route("/all/new/<int:page>")
@limit_pagination
def all_new(page):
    """The index page, all posts sorted as most recent posted first"""
    page_info = misc.getPostList(
        misc.postListQueryBase(isSubMod=current_user.can_admin),
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.all_new",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {},
        }
    )


@bp.route("/domain/<domain>", defaults={"page": None})
@bp.route("/domain/<domain>/<int:page>")
@limit_pagination
def all_domain_new(domain, page):
    """The index page, all posts sorted as most recent posted first"""
    domain = re.sub(r"[^A-Za-z0-9.\-_]+", "", domain)
    page_info = misc.getPostList(
        misc.postListQueryBase(noAllFilter=True).where(
            SubPost.link % ("%://" + domain + "/%")
        ),
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.all_domain_new",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {"domain": domain},
        }
    )


@bp.route("/search/<term>", defaults={"page": None})
@bp.route("/search/<term>/<int:page>")
@ratelimit(POSTING_LIMIT)
@limit_pagination
def search(page, term):
    """The index page, with basic title search"""
    term = re.sub(r'[^A-Za-z0-9.,\-_\'" ]+', "", term)
    page_info = misc.getPostList(
        misc.postListQueryBase().where(SubPost.title ** ("%" + term + "%")),
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.search",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {"term": term},
        }
    )


@bp.route("/all/top", defaults={"page": None})
@bp.route("/all/top/<int:page>")
@limit_pagination
def all_top(page):
    """The index page, all posts sorted as most recent posted first"""
    page_info = misc.getPostList(
        misc.postListQueryBase(isSubMod=current_user.can_admin, sort="top"),
        page=page,
        sort="top",
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )
    return engine.get_template("index.html").render(
        {
            "route": "home.all_top",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {},
        }
    )


@bp.route("/all", defaults={"page": None})
@bp.route("/all/hot", defaults={"page": None})
@bp.route("/all/hot/<int:page>")
@limit_pagination
def all_hot(page):
    """The index page, all posts sorted as most recent posted first"""
    page_info = misc.getPostList(
        misc.postListQueryBase(isSubMod=current_user.can_admin, sort="hot"),
        page=page,
        sort="hot",
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
    )

    return engine.get_template("index.html").render(
        {
            "route": "home.all_hot",
            "page_info": page_info,
            "subOfTheDay": misc.getSubOfTheDay(),
            "changeLog": misc.getChangelog(),
            "ann": misc.getAnnouncement(),
            "kw": {},
        }
    )


# Note for future self: I rewrote until this part. You should do the rest.


@bp.route("/subs", defaults={"page": 1, "sort": "name_asc"})
@bp.route("/subs/<sort>", defaults={"page": 1})
@bp.route("/subs/<int:page>", defaults={"sort": "name_asc"})
@bp.route("/subs/<int:page>/<sort>")
def view_subs(page, sort):
    """Here we can view available subs"""
    c = Sub.select(
        Sub.sid, Sub.name, Sub.title, Sub.nsfw, Sub.creation, Sub.subscribers, Sub.posts
    )

    # sorts...
    if sort == "name_desc":
        c = c.order_by(Sub.name.desc())
    elif sort == "name_asc":
        c = c.order_by(Sub.name.asc())
    elif sort == "posts_desc":
        c = c.order_by(Sub.posts.desc())
    elif sort == "posts_asc":
        c = c.order_by(Sub.posts.asc())
    elif sort == "subs_desc":
        c = c.order_by(Sub.subscribers.desc())
    elif sort == "subs_asc":
        c = c.order_by(Sub.subscribers.asc())
    else:
        return redirect(url_for("home.view_subs", page=page, sort="name_asc"))

    c = c.paginate(page, 50).dicts()
    cp_uri = "/subs/" + str(page)
    return render_template(
        "subs.html", page=page, subs=c, nav="home.view_subs", sort=sort, cp_uri=cp_uri
    )


@bp.route("/subs/search/<term>", defaults={"page": 1, "sort": "name_asc"})
@bp.route("/subs/search/<term>/<sort>", defaults={"page": 1})
@bp.route("/subs/search/<term>/<int:page>", defaults={"sort": "name_asc"})
@bp.route("/subs/search/<term>/<int:page>/<sort>")
@ratelimit(POSTING_LIMIT)
def subs_search(page, term, sort):
    """The subs index page, with basic title search"""
    term = re.sub(r"[^A-Za-z0-9\-_]+", "", term)
    c = Sub.select(
        Sub.sid, Sub.name, Sub.title, Sub.nsfw, Sub.creation, Sub.subscribers, Sub.posts
    )

    c = c.where(Sub.name.contains(term))

    # sorts...
    if sort == "name_desc":
        c = c.order_by(Sub.name.desc())
    elif sort == "name_asc":
        c = c.order_by(Sub.name.asc())
    elif sort == "posts_desc":
        c = c.order_by(Sub.posts.desc())
    elif sort == "posts_asc":
        c = c.order_by(Sub.posts.asc())
    elif sort == "subs_desc":
        c = c.order_by(Sub.subscribers.desc())
    elif sort == "subs_asc":
        c = c.order_by(Sub.subscribers.asc())
    else:
        return redirect(url_for("home.view_subs", page=page, sort="name_asc"))
    c = c.paginate(page, 50).dicts()
    cp_uri = "/subs/search/" + term + "/" + str(page)
    return render_template(
        "subs.html",
        page=page,
        subs=c,
        nav="home.subs_search",
        term=term,
        sort=sort,
        cp_uri=cp_uri,
    )
