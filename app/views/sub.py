""" All endpoints related to stuff done inside of a particular sub """
import datetime
from flask import Blueprint, redirect, url_for, abort, request, Response
from flask_login import login_required, current_user
from feedgen.feed import FeedGenerator
from peewee import fn, JOIN
from ..config import config
from ..misc import engine, limit_pagination
from ..models import (
    Sub,
    SubMetadata,
    SubLog,
    User,
    SubMod,
    SubBan,
    SubUserFlairChoice,
)
from ..forms import (
    EditSubFlair,
    EditSubForm,
    EditMod2Form,
    BanUserSubForm,
    CreateSubFlair,
    AssignSubUserFlair,
)
from .. import misc


blueprint = Blueprint("sub", __name__)


@blueprint.route("/<sub>/")
@blueprint.route("/<sub>")
def view_sub(sub):
    """Here we can view subs"""
    if sub.lower() == "all":
        return redirect(url_for("home.all_hot", page=1))

    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    try:
        x = SubMetadata.select().where(SubMetadata.sid == sub.sid)
        x = x.where(SubMetadata.key == "sort").get()
        x = x.value
    except SubMetadata.DoesNotExist:
        x = "v"
    if x == "v_two":
        return redirect(url_for("sub.view_sub_new", sub=sub.name))
    elif x == "v_three":
        return redirect(url_for("sub.view_sub_top", sub=sub.name))
    else:
        return redirect(url_for("sub.view_sub_hot", sub=sub.name))


@blueprint.route("/<sub>/edit/flairs")
@login_required
def edit_sub_flairs(sub):
    # Implemented by the modmail server.  This stub exists for use by url_for.
    abort(404)


@blueprint.route("/<sub>/edit/user_flairs")
@login_required
def edit_sub_user_flairs(sub):
    """Here we manage user flairs"""
    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    if not current_user.is_mod(sub.sid, 1) and not current_user.is_admin():
        abort(403)

    flairs = SubUserFlairChoice.select().where(SubUserFlairChoice.sub == sub.sid)
    # assigned_flairs = SubUserFlair.select(SubUserFlair.flair_choice, SubUserFlair.flair, User.name)\
    #     .join(User).where(SubUserFlair.sub == sub.sid).dicts()

    assignflair = AssignSubUserFlair()

    formflairs = []
    for flair in flairs:
        formflairs.append(EditSubFlair(flair=flair.id, text=flair.flair))
        assignflair.flair_id.choices.append((flair.id, flair.flair))

    # formuserflairs = []
    # for flair in assigned_flairs:
    #     formuserflairs.append(EditSubUserFlair(flair=flair['flair_choice'], text=flair['flair'], user=flair['name']))

    return engine.get_template("sub/user_flairs.html").render(
        {
            "sub": sub,
            "flairs": formflairs,
            "createflair": CreateSubFlair(),  # 'assigned_flairs': formuserflairs,
            "assignflair": assignflair,
        }
    )


@blueprint.route("/<sub>/edit/rules")
@login_required
def edit_sub_rules(sub):
    # Implemented by the modmail server.  This stub exists for use by url_for.
    abort(404)


@blueprint.route("/<sub>/edit")
@login_required
def edit_sub(sub):
    """Here we can edit sub info and settings"""
    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    if current_user.is_mod(sub.sid, 1) or current_user.is_admin():
        submeta = misc.metadata_to_dict(
            SubMetadata.select().where(SubMetadata.sid == sub.sid)
        )
        form = EditSubForm()
        # pre-populate the form.
        form.subsort.data = submeta.get("sort")
        form.sidebar.data = sub.sidebar
        form.title.data = sub.title

        return engine.get_template("sub/settings.html").render(
            {"sub": sub, "editsubform": form, "metadata": submeta}
        )
    else:
        abort(403)


@blueprint.route("/<sub>/sublog", defaults={"page": 1})
@blueprint.route("/<sub>/sublog/<int:page>")
def view_sublog(sub, page):
    """Here we can see a log of mod/admin activity in the sub"""
    try:
        sub = Sub.select().where(fn.Lower(Sub.name) == sub.lower()).dicts().get()
    except Sub.DoesNotExist:
        abort(404)

    subInfo = misc.getSubData(sub["sid"])
    if not config.site.force_sublog_public:
        log_is_private = subInfo.get("sublog_private", 0) == "1"

        if log_is_private and not (
            current_user.is_mod(sub["sid"], 1) or current_user.is_admin()
        ):
            abort(404)

    subMods = misc.getSubMods(sub["sid"])
    logs = (
        SubLog.select()
        .where(SubLog.sid == sub["sid"])
        .order_by(SubLog.lid.desc())
        .paginate(page, 50)
    )
    return engine.get_template("sub/log.html").render(
        {"sub": sub, "logs": logs, "page": page, "subInfo": subInfo, "subMods": subMods}
    )


@blueprint.route("/<sub>/mods")
@login_required
def edit_sub_mods(sub):
    """Here we can edit moderators for a sub"""
    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    if (
        current_user.is_mod(sub.sid, 2)
        or current_user.is_modinv(sub.sid)
        or current_user.is_admin()
    ):
        subdata = misc.getSubData(sub.sid, extra=True)
        subMods = misc.getSubMods(sub.sid)
        modInvites = (
            SubMod.select(User.name, SubMod.power_level)
            .join(User)
            .where((SubMod.sid == sub.sid) & SubMod.invite)
        )
        return engine.get_template("sub/mods.html").render(
            {
                "sub": sub,
                "subdata": subdata,
                "editmod2form": EditMod2Form(),
                "subMods": subMods,
                "subModInvites": modInvites,
            }
        )
    else:
        abort(403)


@blueprint.route("/<sub>/new.rss")
def sub_new_rss(sub):
    """RSS feed for /sub/new"""
    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    fg = FeedGenerator()
    fg.id(request.url)
    fg.title("New posts from " + sub.name)
    fg.link(href=request.url_root, rel="alternate")
    fg.link(href=request.url, rel="self")

    posts = misc.getPostList(
        misc.postListQueryBase(noAllFilter=True).where(Sub.sid == sub.sid)
    )["posts"]

    return Response(
        misc.populate_feed(fg, posts).atom_str(pretty=True),
        mimetype="application/atom+xml",
    )


@blueprint.route("/<sub>/new", defaults={"page": None})
@blueprint.route("/<sub>/new/<int:page>")
@limit_pagination
def view_sub_new(sub, page):
    """The index page, all posts sorted as most recent posted first"""
    if sub.lower() == "all":
        return redirect(url_for("home.all_new"))

    flair = request.args.get("flair")

    try:
        sub = Sub.select().where(fn.Lower(Sub.name) == sub.lower()).dicts().get()
    except Sub.DoesNotExist:
        abort(404)

    isSubMod = current_user.is_mod(sub["sid"], 1) or current_user.is_admin()

    page_info = misc.getPostList(
        misc.postListQueryBase(
            noAllFilter=True, isSubMod=isSubMod, flair=flair, sort="new"
        ).where(Sub.sid == sub["sid"]),
        sort="new",
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
        include_mod_counts=isSubMod,
    )

    return engine.get_template("sub.html").render(
        {
            "sub": sub,
            "subInfo": misc.getSubData(sub["sid"]),
            "isSubMod": isSubMod,
            "page_info": page_info,
            "route": "sub.view_sub_new",
            "subMods": misc.getSubMods(sub["sid"]),
            "flair": flair,
        }
    )


@blueprint.route("/<sub>/bannedusers")
def view_sub_bans(sub):
    """See banned users for the sub"""
    try:
        sub = Sub.get(fn.Lower(Sub.name) == sub.lower())
    except Sub.DoesNotExist:
        abort(404)

    subInfo = misc.getSubData(sub.sid)
    if not config.site.force_sublog_public:
        banned_users_is_private = subInfo.get("sub_banned_users_private", 0) == "1"

        if banned_users_is_private and not (
            current_user.is_mod(sub.sid, 1) or current_user.is_admin()
        ):
            abort(404)

    user = User.alias()
    created_by = User.alias()
    banned = SubBan.select(
        user, created_by, SubBan.created, SubBan.reason, SubBan.expires
    )
    banned = (
        banned.join(user, on=SubBan.uid)
        .switch(SubBan)
        .join(created_by, JOIN.LEFT_OUTER, on=SubBan.created_by_id)
    )
    banned = (
        banned.where(SubBan.sid == sub.sid)
        .where(SubBan.effective)
        .where(
            (SubBan.expires.is_null(True))
            | (SubBan.expires > datetime.datetime.utcnow())
        )
    )
    banned = banned.order_by(SubBan.created.is_null(True), SubBan.created.desc())

    xbans = SubBan.select(
        user, created_by, SubBan.created, SubBan.reason, SubBan.expires
    )
    xbans = (
        xbans.join(user, on=SubBan.uid)
        .switch(SubBan)
        .join(created_by, JOIN.LEFT_OUTER, on=SubBan.created_by_id)
    )

    xbans = xbans.where(SubBan.sid == sub.sid)
    xbans = xbans.where(
        (~SubBan.effective)
        | (
            (SubBan.expires.is_null(False))
            & (SubBan.expires < datetime.datetime.utcnow())
        )
    )
    xbans = xbans.order_by(
        SubBan.created.is_null(True), SubBan.created.desc(), SubBan.expires.asc()
    )

    return engine.get_template("sub/bans.html").render(
        {
            "sub": sub,
            "banned": banned,
            "xbans": xbans,
            "banuserform": BanUserSubForm(),
            "submods": misc.getSubMods(sub.sid),
        }
    )


@blueprint.route("/<sub>/top", defaults={"page": None})
@blueprint.route("/<sub>/top/<int:page>")
@limit_pagination
def view_sub_top(sub, page):
    """The index page, /top sorting"""
    if sub.lower() == "all":
        return redirect(url_for("home.all_top", page=1))

    flair = request.args.get("flair")

    try:
        sub = Sub.select().where(fn.Lower(Sub.name) == sub.lower()).dicts().get()
    except Sub.DoesNotExist:
        abort(404)

    isSubMod = current_user.is_mod(sub["sid"], 1) or current_user.is_admin()

    page_info = misc.getPostList(
        misc.postListQueryBase(
            noAllFilter=True,
            isSubMod=isSubMod,
            flair=flair,
            sort="top",
        ).where(Sub.sid == sub["sid"]),
        sort="top",
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
        include_mod_counts=isSubMod,
    )

    return engine.get_template("sub.html").render(
        {
            "sub": sub,
            "subInfo": misc.getSubData(sub["sid"]),
            "isSubMod": isSubMod,
            "page_info": page_info,
            "route": "sub.view_sub_top",
            "subMods": misc.getSubMods(sub["sid"]),
            "flair": flair,
        }
    )


@blueprint.route("/<sub>/hot", defaults={"page": None})
@blueprint.route("/<sub>/hot/<int:page>")
@limit_pagination
def view_sub_hot(sub, page):
    """The index page, /hot sorting"""
    if sub.lower() == "all":
        return redirect(url_for("home.all_hot", page=1))

    flair = request.args.get("flair")

    try:
        sub = Sub.select().where(fn.Lower(Sub.name) == sub.lower()).dicts().get()
    except Sub.DoesNotExist:
        abort(404)

    isSubMod = current_user.is_mod(sub["sid"], 1) or current_user.is_admin()

    page_info = misc.getPostList(
        misc.postListQueryBase(
            noAllFilter=True,
            isSubMod=isSubMod,
            flair=flair,
            sort="hot",
        ).where(Sub.sid == sub["sid"]),
        sort="hot",
        page=page,
        first=request.args.get("first"),
        after=request.args.get("after"),
        last=request.args.get("last"),
        before=request.args.get("before"),
        include_mod_counts=isSubMod,
    )

    return engine.get_template("sub.html").render(
        {
            "sub": sub,
            "subInfo": misc.getSubData(sub["sid"]),
            "isSubMod": isSubMod,
            "page_info": page_info,
            "route": "sub.view_sub_hot",
            "subMods": misc.getSubMods(sub["sid"]),
            "flair": flair,
        }
    )


@blueprint.route("/<sub>/<int:pid>", defaults={"slug": ""})
@blueprint.route("/<sub>/<int:pid>/<slug>")
def view_post(sub, pid, slug=None, comments=False, highlight=None):
    # Only a stub, here so routing works.
    return abort(404)


@blueprint.route("/<sub>/<int:pid>/_/<cid>", defaults={"slug": "_"})
@blueprint.route("/<sub>/<int:pid>/<slug>/<cid>")
def view_perm(sub, pid, slug, cid):
    # Only a stub, here so routing works.
    return abort(404)
