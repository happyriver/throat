""" Database and storage related functions and classes """
from base64 import urlsafe_b64encode, urlsafe_b64decode
import binascii
import datetime
import time
import logging
from enum import IntEnum
import functools
import sys
from flask import g, current_app
from flask_redis import FlaskRedis
from peewee import IntegerField, DateTimeField, BooleanField, Proxy, Model, Database
from peewee import CharField, ForeignKeyField, TextField, AutoField
from peewee import BigAutoField, fn, SQL, NodeList
from werkzeug.local import LocalProxy
from .storage import file_url
from .config import config

rconn = FlaskRedis()

dbp = Proxy()


def get_db():
    if "db" not in g:
        if dbp.is_closed():
            current_app.logger.info("database - connect")
            dbp.connect()
        g.db = dbp
    return g.db


db = LocalProxy(get_db)


def db_connect(dbconnect):
    # Taken from peewee's flask_utils
    try:
        name = dbconnect.pop("name")
        engine = dbconnect.pop("engine")
    except KeyError:
        raise RuntimeError("DATABASE configuration must specify a `name` and `engine`.")

    if "." in engine:
        path, class_name = engine.rsplit(".", 1)
    else:
        path, class_name = "peewee", engine

    try:
        __import__(path)
        module = sys.modules[path]
        database_class = getattr(module, class_name)
        assert issubclass(database_class, Database)
    except ImportError:
        raise RuntimeError("Unable to import %s" % engine)
    except AttributeError:
        raise RuntimeError("Database engine not found %s" % engine)
    except AssertionError:
        raise RuntimeError(
            "Database engine not a subclass of peewee.Database: %s" % engine
        )

    if "Pooled" in class_name:
        import logging

        pool_logger = logging.getLogger("peewee.pool")

        class ConnectLoggerDatabase(database_class):
            def _connect(self, **kwargs):
                retval = super(database_class, self)._connect(**kwargs)
                pool_logger.debug("Using connection %s", id(retval))
                return retval

        dbm = ConnectLoggerDatabase(name, **dbconnect)
    else:
        dbm = database_class(name, **dbconnect)
    dbm.execute_sql = functools.partial(peewee_count_queries, dbm.execute_sql)
    return dbm


def db_init_app(app):
    dbconnect = app.config["THROAT_CONFIG"].database.as_dict()
    dbm = db_connect(dbconnect)
    dbp.initialize(dbm)

    @app.teardown_appcontext
    def close_db(exc):
        db = g.pop("db", None)
        if db is not None:
            try:
                db.close()
                app.logger.info("database - closed")
            except Exception as e:
                app.logger.error(f"database - close failed {e}")


#        try:
#            dbp.close_stale()
#        except AttributeError:
#            pass


timing_logger = logging.getLogger("app.sql_timing")


def peewee_count_queries(dex, sql, *args, **kwargs):
    """Used to count and display number of queries"""
    try:
        if not hasattr(g, "pqc"):
            g.pqc = 0
        g.pqc += 1
    except RuntimeError:
        pass
    starttime = time.time()
    try:
        result = dex(sql, *args, **kwargs)
    finally:
        timing_logger.debug(
            "(%s, %s) (executed in %s ms)",
            sql,
            args[0] if len(args) > 0 else kwargs.get("params"),
            int((time.time() - starttime) * 1000),
        )
    return result


class BaseModel(Model):
    @classmethod
    def cursor(cls, sort):
        """Return a list of fields to use in pagination ordering.
        Implementations must return at least one, and currently the
        pagination code can handle at most two. Fields should be
        integers.

        """
        pass

    @classmethod
    def aliased_cursor_fields(cls, sort=None):
        """Give aliases to the pagination cursor fields so they can be
        used in a select statement."""
        fields = cls.cursor(sort)
        return [field.alias("cursor" + str(i)) for i, field in enumerate(fields)]

    @classmethod
    def query_sort(cls, query, sort, forward_direction=True):
        """Add the appropriate sort clause and direction to a query."""
        ordering = cls.cursor(sort)
        if forward_direction:
            return query.order_by(*[field.desc() for field in ordering])
        else:
            return query.order_by(*[field.asc() for field in ordering])

    @classmethod
    def offset(cls, query, cursor, sort, forward_direction):
        """Add a where clause to discard items before or after a
        cursor.  Moving forward is in the direction of decreasing
        values.

        """
        try:
            cursor = cls.decode_cursor(cursor)
        except (UnicodeDecodeError, binascii.Error, ValueError):
            return query

        cursor_fields = cls.cursor(sort)
        if forward_direction:
            condition = cursor_fields[0] < cursor[0]
            if len(cursor_fields) > 1:
                condition = condition | (
                    (cursor_fields[0] == cursor[0]) & (cursor_fields[1] < cursor[1])
                )
        else:
            condition = cursor_fields[0] > cursor[0]
            if len(cursor_fields) > 1:
                condition = condition | (
                    (cursor_fields[0] == cursor[0]) & (cursor_fields[1] > cursor[1])
                )
        return query.where(condition)

    @staticmethod
    def encode_cursor(item):
        """Base64 encode a cursor using the cursor field names created
        by aliased_cursor_fields.  'item' should be a dictionary.

        """
        cursor = [str(item["cursor0"])]
        if item.get("cursor1") is not None:
            cursor = cursor + [str(item["cursor1"])]
        cursor = "/".join(cursor)
        return urlsafe_b64encode(cursor.encode("utf-8")).split(b"=")[0].decode("utf-8")

    @staticmethod
    def decode_cursor(cursor):
        """Decode a base64-encoded cursor into one or more field values."""

        def round_up_by_4s(x):
            return ((x + 3) // 4) * 4 - x

        cursor = cursor.encode("utf-8") + b"=" * round_up_by_4s(len(cursor))
        cursor = urlsafe_b64decode(cursor).decode("utf-8").split("/")
        return [int(elem) for elem in cursor]

    class Meta:
        database = db


class UserCrypto(IntEnum):
    """Password hash algorithm."""

    BCRYPT = 1
    REMOTE = 2  # password stored on remote auth server


class UserStatus(IntEnum):
    """User's login capability status."""

    OK = 0
    PROBATION = 1  # New, with email not yet confirmed.
    BANNED = 5  # site-ban
    DELETED = 10


class User(BaseModel):
    uid = CharField(primary_key=True, max_length=40)
    joindate = DateTimeField(null=True)
    name = CharField(null=True, unique=True, max_length=64)

    score = IntegerField(default=0)  # AKA phuks taken
    given = IntegerField(default=0)  # AKA phuks given
    upvotes_given = IntegerField(null=True)
    downvotes_given = IntegerField(null=True)

    # status: 0 = OK; 10 = deleted; 5 = site-ban
    status = IntegerField(default=0)
    resets = IntegerField(default=0)

    language = CharField(default=None, null=True, max_length=11)

    def __repr__(self):
        return f"<User {self.name}>"

    class Meta:
        table_name = "user"
        users = ["ovarit_auth", "ovarit_subs", "throat_be", "throat_py"]


class SiteLog(BaseModel):
    action = IntegerField(null=True)
    desc = CharField(null=True)
    lid = AutoField()
    link = CharField(null=True)
    time = DateTimeField(default=datetime.datetime.utcnow)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    target = ForeignKeyField(
        column_name="target_uid", null=True, model=User, field="uid"
    )

    def __repr__(self):
        return f"<SiteLog action={self.action}>"

    class Meta:
        table_name = "site_log"
        users = ["throat_be", "throat_py"]


class SiteMetadata(BaseModel):
    key = CharField(null=True)
    value = CharField(null=True)
    xid = AutoField()

    def __repr__(self):
        return f"<SiteMetadata {self.key}>"

    class Meta:
        table_name = "site_metadata"
        users = ["ovarit_auth", "throat_be", "throat_py"]


class Sub(BaseModel):
    name = CharField(unique=True, max_length=32)
    nsfw = BooleanField(default=False)
    sid = CharField(primary_key=True, max_length=40)
    sidebar = TextField(default="")
    status = IntegerField(null=True)
    title = CharField(null=True, max_length=50)
    sort = CharField(null=True, max_length=32)
    creation = DateTimeField(default=datetime.datetime.utcnow)
    subscribers = IntegerField(default=1)
    posts = IntegerField(default=0)

    def __repr__(self):
        return f"<Sub {self.name}>"

    class Meta:
        table_name = "sub"
        users = ["ovarit_auth", "throat_be", "throat_py"]

    def get_metadata(self, key):
        """Returns `key` for submetadata or `None` if it does not exist.
        Only works for single keys"""
        try:
            m = SubMetadata.get(
                (SubMetadata.sid == self.sid) & (SubMetadata.key == key)
            )
            return m.value
        except SubMetadata.DoesNotExist:
            return None

    def update_metadata(self, key, value, boolean=True):
        """Updates `key` for submetadata. Only works for single keys."""
        if boolean:
            if value:
                value = "1"
            elif not value:
                value = "0"
        restr = SubMetadata.get_or_create(sid=self.sid, key=key)[0]
        if restr.value != value:
            restr.value = value
            restr.save()


class SubFlair(BaseModel):
    sid = ForeignKeyField(column_name="sid", null=True, model=Sub, field="sid")
    text = CharField(null=True)
    order = IntegerField(null=True)
    xid = AutoField()
    mods_only = BooleanField(default=False)

    def __repr__(self):
        return f"<SubFlair {self.text}>"

    class Meta:
        table_name = "sub_flair"
        users = ["throat_be", "throat_py"]


class SubRule(BaseModel):
    sid = ForeignKeyField(column_name="sid", null=True, model=Sub, field="sid")
    text = CharField(null=True)
    order = IntegerField(null=True)
    rid = AutoField()

    def __repr__(self):
        return f"<SubRule {self.text}>"

    class Meta:
        table_name = "sub_rule"
        users = ["throat_be", "throat_py"]


class SubLog(BaseModel):
    action = IntegerField(null=True)
    desc = CharField(null=True)
    lid = AutoField()
    link = CharField(null=True)  # link or extra description depending on action
    sid = ForeignKeyField(column_name="sid", null=True, model=Sub, field="sid")
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    target = ForeignKeyField(
        column_name="target_uid", null=True, model=User, field="uid"
    )
    admin = BooleanField(
        default=False
    )  # True if action was performed by an admin override.
    time = DateTimeField(default=datetime.datetime.utcnow)

    def __repr__(self):
        return f"<SubLog action={self.action}>"

    class Meta:
        table_name = "sub_log"
        users = ["throat_be", "throat_py"]


class SubMetadata(BaseModel):
    key = CharField(null=True)
    sid = ForeignKeyField(column_name="sid", null=True, model=Sub, field="sid")
    value = CharField(null=True)
    xid = AutoField()

    def __repr__(self):
        return f"<SubMetadata {self.key}>"

    class Meta:
        table_name = "sub_metadata"
        users = ["throat_be", "throat_py"]


class SubPost(BaseModel):
    content = TextField(null=True)
    deleted = IntegerField(
        default=0
    )  # 1=self delete, 2=mod delete, 3=admin delete, 0=not deleted
    distinguish = IntegerField(null=True)  # 1=mod, 2=admin, 0 or null = normal
    link = CharField(null=True)
    nsfw = BooleanField(null=True)
    pid = AutoField()
    posted = DateTimeField(default=datetime.datetime.utcnow)
    edited = DateTimeField(null=True)
    ptype = IntegerField(null=True)  # 0=text, 1=link, 3=poll

    score = IntegerField(default=1)  # XXX: Deprecated
    upvotes = IntegerField(default=0)
    downvotes = IntegerField(default=0)

    sid = ForeignKeyField(column_name="sid", model=Sub, field="sid")
    thumbnail = CharField(null=True)
    title = CharField()
    slug = CharField()
    comments = IntegerField()
    uid = ForeignKeyField(column_name="uid", model=User, field="uid", backref="posts")
    flair = CharField(null=True, max_length=25)

    def __repr__(self):
        return f'<SubPost "{self.title[:20]}">'

    @classmethod
    def cursor(cls, sort):
        """Return the model fields to sort on, by sort type."""
        if sort == "top":
            return [cls.score, cls.pid]
        elif sort == "hot":
            return [cls.hot_score()]
        else:
            return [cls.pid]

    @classmethod
    def hot_score(cls):
        """Return an expression to calculate a post's hot score."""
        posted = fn.date_part("epoch", cls.posted)
        if config.site.custom_hot_sort:
            return fn.HOT(cls.score, posted, python_value=float)
        else:
            return (cls.score * 20 + (posted - 1134028003) / 1500).cast("int")

    @staticmethod
    def decode_cursor(cursor):
        """Decode a base64-encoded cursor into one or more field values."""

        def round_up_by_4s(x):
            return ((x + 3) // 4) * 4 - x

        cursor = cursor.encode("utf-8") + b"=" * round_up_by_4s(len(cursor))
        cursor = urlsafe_b64decode(cursor).decode("utf-8").split("/")
        if len(cursor) == 1:
            return [float(cursor[0]) if "." in cursor[0] else int(cursor[0])]
        else:
            return [int(cursor[0]), int(cursor[1])]

    class Meta:
        table_name = "sub_post"
        users = ["throat_be", "throat_py"]


class SubPostPollOption(BaseModel):
    """List of options for a poll"""

    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")
    text = CharField()

    def __repr__(self):
        return f'<SubPostPollOption "{self.text[:20]}">'

    class Meta:
        table_name = "sub_post_poll_option"
        users = ["throat_be", "throat_py"]


class SubPostPollVote(BaseModel):
    """List of options for a poll"""

    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")
    uid = ForeignKeyField(column_name="uid", model=User)
    vid = ForeignKeyField(column_name="vid", model=SubPostPollOption, backref="votes")

    def __repr__(self):
        return "<SubPostPollVote>"

    class Meta:
        table_name = "sub_post_poll_vote"
        users = ["throat_be", "throat_py"]


class SubPostComment(BaseModel):
    cid = CharField(primary_key=True, max_length=40)
    content = TextField(null=True)
    lastedit = DateTimeField(null=True)
    parentcid = ForeignKeyField(
        column_name="parentcid", null=True, model="self", field="cid"
    )
    pid = ForeignKeyField(column_name="pid", null=True, model=SubPost, field="pid")
    score = IntegerField(null=True)
    upvotes = IntegerField(default=0)
    downvotes = IntegerField(default=0)
    views = IntegerField(default=0)

    # status:
    #   null or 0: Either not deleted, or reinstated.
    #   1:         The user removed it themselves; still visible to mods, admins.
    #   2:         A mod removed it; still visible to mods, admins and the user.
    #   3:         An admin removed it; still visible to mods, admins and the user.
    status = IntegerField(null=True)
    distinguish = IntegerField(null=True)  # 1=mod, 2=admin, 0 or null = normal
    time = DateTimeField(null=True)
    uid = ForeignKeyField(
        column_name="uid", null=True, model=User, field="uid", backref="comments"
    )

    @classmethod
    def cursor(cls, sort=None):
        """Return the model field to sort on, by sort type."""
        return [fn.EXTRACT(NodeList((SQL("EPOCH FROM"), cls.time))).cast("int")]

    def __repr__(self):
        return f'<SubPostComment "{self.content[:20]}">'

    class Meta:
        table_name = "sub_post_comment"
        users = ["throat_be", "throat_py"]


class SubPostCommentVote(BaseModel):
    datetime = DateTimeField(null=True, default=datetime.datetime.utcnow)
    cid = CharField(null=True)
    positive = IntegerField(null=True)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    xid = AutoField()

    def __repr__(self):
        return f"<SubPostCommentVote {self.cid}>"

    class Meta:
        table_name = "sub_post_comment_vote"
        users = ["throat_be", "throat_py"]


class SubPostView(BaseModel):
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")
    datetime = DateTimeField(null=True, default=datetime.datetime.utcnow)

    def __repr__(self):
        return f"<SubPostView{self.cid}>"

    class Meta:
        table_name = "sub_post_view"
        users = ["throat_be", "throat_py"]


class SubPostCommentView(BaseModel):
    cid = ForeignKeyField(column_name="cid", model=SubPostComment, field="cid")
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")

    def __repr__(self):
        return f'<SubPostCommentView "{self.cid}">'

    class Meta:
        table_name = "sub_post_comment_view"
        users = ["throat_be", "throat_py"]


class SubPostMetadata(BaseModel):
    key = CharField(null=True)
    pid = ForeignKeyField(column_name="pid", null=True, model=SubPost, field="pid")
    value = CharField(null=True)
    xid = AutoField()

    def __repr__(self):
        return f"<SubPostMetadata {self.key}>"

    class Meta:
        table_name = "sub_post_metadata"
        users = ["throat_be", "throat_py"]


class SubPostVote(BaseModel):
    datetime = DateTimeField(null=True, default=datetime.datetime.utcnow)
    pid = ForeignKeyField(
        column_name="pid", null=True, model=SubPost, field="pid", backref="votes"
    )
    positive = IntegerField(null=True)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    xid = AutoField()

    def __repr__(self):
        return f"<SubPostVote {self.positive}>"

    class Meta:
        table_name = "sub_post_vote"
        users = ["throat_be", "throat_py"]


class SubSubscriber(BaseModel):
    """Stores subscribed and blocked subs"""

    order = IntegerField(null=True)
    sid = ForeignKeyField(column_name="sid", null=True, model=Sub, field="sid")
    # status is 1 for subscribed, 2 for blocked and 4 for saved (displayed in the top bar)
    status = IntegerField(null=True)
    time = DateTimeField(default=datetime.datetime.utcnow)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    xid = AutoField()

    def __repr__(self):
        return f"<SubSubscriber {self.status}>"

    class Meta:
        table_name = "sub_subscriber"
        users = ["ovarit_auth", "throat_be", "throat_py"]


class UserMetadata(BaseModel):
    key = CharField(null=True)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    value = CharField(null=True)
    xid = AutoField()

    def __repr__(self):
        return f"<UserMetadata {self.key}>"

    class Meta:
        table_name = "user_metadata"
        users = ["ovarit_auth", "ovarit_subs", "throat_be", "throat_py"]


class Badge(BaseModel):
    bid = AutoField()
    # supercalifragilisticexpialidocious == 34
    name = CharField(unique=True, max_length=34)
    alt = CharField(max_length=255)
    icon = CharField()
    score = IntegerField()
    rank = IntegerField()
    trigger = CharField(null=True)

    def __getitem__(self, key):
        tmp = self.__dict__.get(key)
        if key == "icon":
            tmp = file_url(tmp)
        return tmp

    def icon_url(self):
        return file_url(self.icon)

    class Meta:
        table_name = "badge"
        users = ["ovarit_subs", "throat_be", "throat_py"]


class UserSaved(BaseModel):
    pid = IntegerField(null=True)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    xid = AutoField()

    def __repr__(self):
        return f"<UserSaved {self.uid}>"

    class Meta:
        table_name = "user_saved"
        users = ["throat_be", "throat_py"]


class UserUploads(BaseModel):
    xid = AutoField()
    pid = ForeignKeyField(column_name="pid", null=True, model=SubPost, field="pid")
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    fileid = CharField(null=True)
    thumbnail = CharField(null=True)
    status = IntegerField()

    @classmethod
    def cursor(cls, sort=None):
        """Return the model field to sort on, by sort type."""
        return [cls.xid]

    def __repr__(self):
        return f"<UserUploads {self.fileid}>"

    class Meta:
        table_name = "user_uploads"
        users = ["throat_be", "throat_py"]


class SubPostReport(BaseModel):
    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    datetime = DateTimeField(default=datetime.datetime.now)
    reason = CharField(max_length=128)
    open = BooleanField(default=True)
    send_to_admin = BooleanField(default=True)

    def __repr__(self):
        return f'<SubPostReport "{self.reason[:20]}">'

    class Meta:
        table_name = "sub_post_report"
        users = ["throat_be", "throat_py"]


class PostReportLog(BaseModel):
    rid = ForeignKeyField(column_name="id", model=SubPostReport, field="id")
    action = IntegerField(null=True)
    desc = CharField(null=True)
    lid = AutoField()
    link = CharField(null=True)
    time = DateTimeField(default=datetime.datetime.utcnow)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    target = ForeignKeyField(
        column_name="target_uid", null=True, model=User, field="uid"
    )

    def __repr__(self):
        return f"<PostReportLog action={self.action}>"

    class Meta:
        table_name = "post_report_log"
        users = ["throat_be", "throat_py"]


class SubPostCommentReport(BaseModel):
    cid = ForeignKeyField(column_name="cid", model=SubPostComment, field="cid")
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    datetime = DateTimeField(default=datetime.datetime.now)
    reason = CharField(max_length=128)
    open = BooleanField(default=True)
    send_to_admin = BooleanField(default=True)

    def __repr__(self):
        return f'<SubPostCommentReport "{self.reason[:20]}">'

    class Meta:
        table_name = "sub_post_comment_report"
        users = ["throat_be", "throat_py"]


class CommentReportLog(BaseModel):
    rid = ForeignKeyField(column_name="id", model=SubPostCommentReport, field="id")
    action = IntegerField(null=True)
    desc = CharField(null=True)
    lid = AutoField()
    link = CharField(null=True)
    time = DateTimeField(default=datetime.datetime.utcnow)
    uid = ForeignKeyField(column_name="uid", null=True, model=User, field="uid")
    target = ForeignKeyField(
        column_name="target_uid", null=True, model=User, field="uid"
    )

    def __repr__(self):
        return f"<CommentReportLog action={self.action}>"

    class Meta:
        table_name = "comment_report_log"
        users = ["throat_be", "throat_py"]


class SubPostCommentHistory(BaseModel):
    cid = ForeignKeyField(column_name="cid", model=SubPostComment, field="cid")
    datetime = DateTimeField(default=datetime.datetime.now)
    content = TextField(null=True)

    def __repr__(self):
        return f'<SubPostCommentHistory "{self.content[:20]}">'

    class Meta:
        table_name = "sub_post_comment_history"
        users = ["throat_be", "throat_py"]


class SubPostContentHistory(BaseModel):
    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")
    datetime = DateTimeField(default=datetime.datetime.now)
    content = TextField(null=True)

    def __repr__(self):
        return f'<SubPostContentHistory "{self.content[:20]}">'

    class Meta:
        table_name = "sub_post_content_history"
        users = ["throat_be", "throat_py"]


class SubPostTitleHistory(BaseModel):
    pid = ForeignKeyField(column_name="pid", model=SubPost, field="pid")
    datetime = DateTimeField(default=datetime.datetime.now)
    title = TextField(null=True)
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")

    def __repr__(self):
        return f'<SubPostContentHistory "{self.content[:20]}">'

    class Meta:
        table_name = "sub_post_title_history"
        users = ["throat_be", "throat_py"]


class SubPostCommentCheckoff(BaseModel):
    """Allow mods to check off comments."""

    cid = ForeignKeyField(
        column_name="cid", model=SubPostComment, field="cid", unique=True
    )
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    datetime = DateTimeField(default=datetime.datetime.now)

    def __repr__(self):
        return f'<SubPostCommentCheckoff "{self.id}">'

    class Meta:
        table_name = "sub_post_comment_checkoff"
        users = ["throat_be", "throat_py"]


class UserMessageBlock(BaseModel):
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    target = CharField(max_length=40)
    date = DateTimeField(default=datetime.datetime.now)

    def __repr__(self):
        return f'<UserMessageBlock "{self.id}">'

    class Meta:
        table_name = "user_message_block"
        users = ["throat_be", "throat_py"]


class UserContentBlockMethod(IntEnum):
    """Ways to block content for users.
    Value of the 'method' field in UserContentBlock."""

    HIDE = 0
    BLUR = 1


class UserContentBlock(BaseModel):
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    target = CharField(max_length=40)
    date = DateTimeField(default=datetime.datetime.now)
    method = IntegerField()  # 0=hide, 1=blur

    def __repr__(self):
        return f'<UserContentBlock "{self.id}">'

    class Meta:
        table_name = "user_content_block"
        users = ["throat_be", "throat_py"]


class APIToken(BaseModel):
    user = ForeignKeyField(column_name="uid", model=User, field="uid")
    token = CharField(unique=True)
    can_post = BooleanField()
    can_mod = BooleanField()
    can_message = BooleanField()
    can_upload = BooleanField()
    is_active = BooleanField(default=True)
    is_ip_restricted = BooleanField(default=False)

    def __repr__(self):
        return f"<APIToken {self.token}>"

    class Meta:
        table_name = "api_token"
        users = ["throat_py"]


class APITokenSettings(BaseModel):
    """API Token settings. Mainly used for IP whitelisting"""

    token = ForeignKeyField(model=APIToken, field="id")
    key = CharField()
    value = CharField()

    def __repr__(self):
        return f"<APITokenSettings {self.key}>"

    class Meta:
        table_name = "api_token_settings"
        users = ["throat_py"]


class SubMod(BaseModel):
    user = ForeignKeyField(column_name="uid", model=User, field="uid")
    sub = ForeignKeyField(column_name="sid", model=Sub, field="sid")
    # Power level: 0=owner, 1=mod, 2=janitor
    power_level = IntegerField()

    invite = BooleanField(default=False)  # if True, mod is invited and not effective

    def __repr__(self):
        return f"<SubMod power_level={self.power_level}>"

    class Meta:
        table_name = "sub_mod"
        users = ["throat_be", "throat_py"]


class SubBan(BaseModel):
    user = ForeignKeyField(column_name="uid", model=User, field="uid")
    sub = ForeignKeyField(column_name="sid", model=Sub, field="sid")

    created = DateTimeField(default=datetime.datetime.utcnow)
    reason = CharField(max_length=128)
    expires = DateTimeField(null=True)
    effective = BooleanField(default=True)

    created_by = ForeignKeyField(column_name="created_by_id", model=User, field="uid")

    def __repr__(self):
        return f'<SubBan "{self.reason[:20]}">'

    class Meta:
        table_name = "sub_ban"
        users = ["throat_be", "throat_py"]


class SubUserFlairChoice(BaseModel):
    """
    Stores possible user flair choces for a sub
    """

    sub = ForeignKeyField(column_name="sid", model=Sub, field="sid")
    flair = CharField(null=False, max_length=25)

    class Meta:
        table_name = "sub_user_flair_choice"
        users = ["throat_be", "throat_py"]


class SubUserFlair(BaseModel):
    """
    Stores flairs assigned to users in a Sub
    """

    user = ForeignKeyField(column_name="uid", model=User, field="uid")
    sub = ForeignKeyField(column_name="sid", model=Sub, field="sid")

    flair = CharField(null=False, max_length=25)

    flair_choice = ForeignKeyField(model=SubUserFlairChoice, null=True)

    class Meta:
        table_name = "sub_user_flair"
        users = ["throat_be", "throat_py"]


class InviteCode(BaseModel):
    user = ForeignKeyField(column_name="uid", model=User, field="uid")

    code = CharField(max_length=64)

    created = DateTimeField(default=datetime.datetime.utcnow)
    expires = DateTimeField(null=True)
    uses = IntegerField(default=0)
    max_uses = IntegerField()

    @classmethod
    def get_valid(cls, invite_code):
        """Returns a valid invite code
        @raise InviteCode.DoesNotExist
        """

        return (
            InviteCode.select()
            .join(User)
            .where(InviteCode.code == invite_code.strip())
            .where(
                (
                    InviteCode.expires.is_null()
                    | (InviteCode.expires > datetime.datetime.utcnow())
                )
                & (User.status == UserStatus.OK)
            )
            .where(InviteCode.max_uses > InviteCode.uses)
            .get()
        )

    def __repr__(self):
        return f"<InviteCode {self.code}>"

    class Meta:
        table_name = "invite_code"
        users = ["ovarit_auth", "throat_be", "throat_py"]


class Wiki(BaseModel):
    is_global = BooleanField()
    sub = ForeignKeyField(column_name="sid", model=Sub, field="sid", null=True)

    slug = CharField(max_length=128)
    title = CharField(max_length=255)
    content = TextField()

    created = DateTimeField(default=datetime.datetime.utcnow)
    updated = DateTimeField(default=datetime.datetime.utcnow)

    class Meta:
        table_name = "wiki"
        users = ["throat_be", "throat_py"]


class Notification(BaseModel):
    """Holds user notifications."""

    # Notification type. Can be one of:
    # - POST_REPLY, COMMENT_REPLY
    # - POST_MENTION, COMMENT_MENTION

    # These were used in old notifications, but are now sent as messages.
    # - SUB_BAN, SUB_UNBAN
    # - MOD_INVITE, MOD_INVITE_JANITOR, MOD_INVITE_OWNER
    # - POST_DELETE, POST_UNDELETE
    type = CharField()

    sub = ForeignKeyField(column_name="sid", model=Sub, field="sid", null=True)
    # Post the notification is referencing, if it applies
    post = ForeignKeyField(column_name="pid", model=SubPost, field="pid", null=True)
    # Comment the notification is referring, if it applies
    comment = ForeignKeyField(
        column_name="cid", model=SubPostComment, field="cid", null=True
    )
    # User that triggered the action. If null the action is triggered by the system
    sender = ForeignKeyField(column_name="sentby", model=User, field="uid", null=True)

    target = ForeignKeyField(
        column_name="receivedby", model=User, field="uid", null=True
    )
    read = DateTimeField(null=True)
    # For future custom text notifications sent by admins (badge notifications?)015_notifications
    content = TextField(null=True)

    created = DateTimeField(default=datetime.datetime.utcnow)

    def __repr__(self):
        return f'<Notification target="{self.user}" type="{self.type}" >'

    class Meta:
        table_name = "notification"
        users = ["throat_be", "throat_py"]


class MessageType(IntEnum):
    """Types of direct messages.
    Value of the 'mtype' field in Message."""

    USER_TO_USER = 100
    USER_TO_MODS = 101
    MOD_TO_USER_AS_USER = 102
    MOD_TO_USER_AS_MOD = 103
    MOD_DISCUSSION = 104
    USER_NOTIFICATION = 105
    MOD_NOTIFICATION = 106


class MessageMailbox(IntEnum):
    """Mailboxes for direct messages."""

    INBOX = 200
    SENT = 201
    SAVED = 202
    ARCHIVED = 203  # Modmail only.
    TRASH = 204
    DELETED = 205
    PENDING = 206  # Modmail only.


class MessageThread(BaseModel):
    """Fields shared by all messages in a message thread."""

    mtid = AutoField()
    replies = IntegerField(default=0)
    subject = CharField()
    # Relevant for modmail messages, otherwise NULL.
    sub = ForeignKeyField(column_name="sid", null=True, model=Sub, field="sid")

    def __repr__(self):
        return f'<MessageThread "{self.mtid}"'

    class Meta:
        table_name = "message_thread"
        users = ["throat_be", "throat_py"]


class Message(BaseModel):
    mid = AutoField()
    thread = ForeignKeyField(column_name="mtid", model=MessageThread, field="mtid")
    content = TextField(null=True)
    mtype = IntegerField(null=True)
    posted = DateTimeField(null=True)
    # True if this message is the first in its thread.
    first = BooleanField(default=False)

    # Relevant for messages to individual users, otherwise NULL.
    receivedby = ForeignKeyField(
        column_name="receivedby", null=True, model=User, field="uid"
    )

    # The user who created the message, even for modmails.
    sentby = ForeignKeyField(
        column_name="sentby",
        null=True,
        model=User,
        backref="user_sentby_set",
        field="uid",
    )

    @classmethod
    def cursor(cls, sort=None):
        """Return the model field to sort on, by sort type."""
        return [cls.mid]

    def __repr__(self):
        return f'<Message "{self.mid}"'

    class Meta:
        table_name = "message"
        users = ["throat_be", "throat_py"]


class UserUnreadMessage(BaseModel):
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    mid = ForeignKeyField(column_name="mid", model=Message, field="mid")

    def __repr__(self):
        return f'<UserUnreadMessage "{self.uid}/{self.mid}"'

    class Meta:
        table_name = "user_unread_message"
        users = ["throat_be", "throat_py"]


class UserMessageMailbox(BaseModel):
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    mid = ForeignKeyField(column_name="mid", model=Message, field="mid")
    mailbox = IntegerField(default=MessageMailbox.INBOX)

    def __repr__(self):
        return f'<UserMessageMailbox "{self.uid}/{self.mid}"'

    class Meta:
        table_name = "user_message_mailbox"
        users = ["throat_be", "throat_py"]


class SubMessageMailbox(BaseModel):
    thread = ForeignKeyField(column_name="mtid", model=MessageThread, field="mtid")
    mailbox = IntegerField(default=MessageMailbox.INBOX)
    highlighted = BooleanField(default=False)

    def __repr__(self):
        return f'<SubMessageMailbox "{self.id}"'

    class Meta:
        table_name = "sub_message_mailbox"
        users = ["throat_be", "throat_py"]


class SubMessageLogAction(IntEnum):
    CHANGE_MAILBOX = 1
    HIGHLIGHT = 2


class SubMessageLog(BaseModel):
    action = IntegerField(default=SubMessageLogAction.CHANGE_MAILBOX)
    thread = ForeignKeyField(column_name="mtid", model=MessageThread, field="mtid")
    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    desc = CharField(null=True)
    updated = DateTimeField(default=datetime.datetime.utcnow)

    def __repr__(self):
        return f'<SubMessageLog "{self.id}"'

    class Meta:
        table_name = "sub_message_log"
        users = ["throat_be", "throat_py"]


class Visit(BaseModel):
    id = BigAutoField()
    datetime = DateTimeField(index=True, default=datetime.datetime.utcnow)
    ipaddr = TextField(null=False)

    def __repr__(self):
        return f'<Visit "{self.datetime}"'

    class Meta:
        table_name = "visit"
        users = ["throat_be", "throat_py"]


class CustomerUpdateLog(BaseModel):
    """Record changes in user status that need to be, or have already
    been, updated with Stripe."""

    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    # Possibilities are "change_email" and "cancel_subscription"
    action = TextField()
    value = TextField(null=True)
    created = DateTimeField(default=datetime.datetime.utcnow)
    completed = DateTimeField(null=True)
    success = BooleanField(null=True)

    def __repr__(self):
        return f'<CustomerUpdateLog "{self.uid}"'

    class Meta:
        table_name = "customer_update_log"
        users = ["ovarit_auth", "throat_be", "throat_py"]


class PostTypeConfig(BaseModel):
    """Configuration of each post type permitted in a sub."""

    sid = ForeignKeyField(column_name="sid", model=Sub, field="sid")
    # 0=text, 1=link, 2=upload, 3=poll
    ptype = IntegerField()
    rules = TextField(null=True)
    mods_only = BooleanField(default=False)

    def __repr__(self):
        return f'<PostTypeConfig> "{self.sid}/{self.ptype}"'

    class Meta:
        table_name = "post_type_config"
        users = ["throat_be", "throat_py"]


class DisallowFlairPostType(BaseModel):
    """Support restricting flairs to only certain post types.

        A flair may not be used on a post type if a corresponding row
    exists in this table.
    """

    flair_id = ForeignKeyField(column_name="flair_id", model=SubFlair, field="xid")
    # 0=text, 1=link, 2=upload, 3=poll
    ptype = IntegerField()

    class Meta:
        table_name = "disallow_flair_post_type"
        users = ["throat_be", "throat_py"]


class UserNameHistory(BaseModel):
    """Store a user's history of user names."""

    uid = ForeignKeyField(column_name="uid", model=User, field="uid")
    name = CharField(null=False, max_length=64)
    changed = DateTimeField(default=datetime.datetime.utcnow)
    required_by_admin = BooleanField(default=False)

    def __repr__(self):
        return f'<UserNameHistory> "{self.id}/{self.name}"'

    class Meta:
        table_name = "username_history"
        users = ["throat_be", "throat_py", "ovarit_auth"]
