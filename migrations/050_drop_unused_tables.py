""""Peewee migrations -- 050_drop_unused_tables.py

Remove some unused tables from the schema.
"""
import datetime as dt
import json
import peewee as pw
import playhouse.postgres_ext as pw_ext

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    migrator.remove_model("client")
    migrator.remove_model("grant")
    migrator.remove_model("token")


def rollback(migrator, database, fake=False, **kwargs):
    @migrator.create_model
    class Client(pw.Model):
        client = pw.CharField(column_name="client_id", max_length=40, primary_key=True)
        _default_scopes = pw.TextField(null=True)
        _redirect_uris = pw.TextField(null=True)
        client_secret = pw.CharField(max_length=55, unique=True)
        is_confidential = pw.BooleanField(null=True)
        name = pw.CharField(max_length=40, null=True)
        user = pw.ForeignKeyField(
            backref="client_set",
            column_name="user_id",
            field="uid",
            model=migrator.orm["user"],
            null=True,
        )

        class Meta:
            table_name = "client"

    @migrator.create_model
    class Grant(pw.Model):
        id = pw.AutoField()
        _scopes = pw.TextField(null=True)
        client = pw.ForeignKeyField(
            backref="grant_set",
            column_name="client_id",
            field="client",
            model=migrator.orm["client"],
        )
        code = pw.CharField(index=True, max_length=255)
        expires = pw.DateTimeField(null=True)
        redirect_uri = pw.CharField(max_length=255, null=True)
        user = pw.ForeignKeyField(
            backref="grant_set",
            column_name="user_id",
            field="uid",
            model=migrator.orm["user"],
            null=True,
        )

        class Meta:
            table_name = "grant"

    @migrator.create_model
    class Token(pw.Model):
        id = pw.AutoField()
        _scopes = pw.TextField(null=True)
        access_token = pw.CharField(max_length=100, null=True, unique=True)
        client = pw.ForeignKeyField(
            backref="token_set",
            column_name="client_id",
            field="client",
            model=migrator.orm["client"],
        )
        expires = pw.DateTimeField(null=True)
        refresh_token = pw.CharField(max_length=100, null=True, unique=True)
        token_type = pw.CharField(max_length=40, null=True)
        user = pw.ForeignKeyField(
            backref="token_set",
            column_name="user_id",
            field="uid",
            model=migrator.orm["user"],
            null=True,
        )

        class Meta:
            table_name = "token"
