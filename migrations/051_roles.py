""""Peewee migrations -- 051_roles.py

Grant permissions to database users.
"""

table_permissions = {
    "api_token": ["throat_py"],
    "api_token_settings": ["throat_py"],
    "badge": ["ovarit_subs", "throat_be", "throat_py"],
    "comment_report_log": ["throat_be", "throat_py"],
    "invite_code": ["ovarit_auth", "throat_be", "throat_py"],
    "message": ["throat_be", "throat_py"],
    "message_thread": ["throat_be", "throat_py"],
    "notification": ["throat_be", "throat_py"],
    "post_report_log": ["throat_be", "throat_py"],
    "site_log": ["throat_be", "throat_py"],
    "site_metadata": ["ovarit_auth", "throat_be", "throat_py"],
    "sub": ["ovarit_auth", "throat_be", "throat_py"],
    "sub_ban": ["throat_be", "throat_py"],
    "sub_flair": ["throat_be", "throat_py"],
    "sub_log": ["throat_be", "throat_py"],
    "sub_message_log": ["throat_be", "throat_py"],
    "sub_message_mailbox": ["throat_be", "throat_py"],
    "sub_metadata": ["throat_be", "throat_py"],
    "sub_mod": ["throat_be", "throat_py"],
    "sub_post": ["throat_be", "throat_py"],
    "sub_post_comment": ["throat_be", "throat_py"],
    "sub_post_comment_checkoff": ["throat_be", "throat_py"],
    "sub_post_comment_history": ["throat_be", "throat_py"],
    "sub_post_comment_report": ["throat_be", "throat_py"],
    "sub_post_comment_view": ["throat_be", "throat_py"],
    "sub_post_comment_vote": ["throat_be", "throat_py"],
    "sub_post_content_history": ["throat_be", "throat_py"],
    "sub_post_metadata": ["throat_be", "throat_py"],
    "sub_post_poll_option": ["throat_be", "throat_py"],
    "sub_post_poll_vote": ["throat_be", "throat_py"],
    "sub_post_report": ["throat_be", "throat_py"],
    "sub_post_title_history": ["throat_be", "throat_py"],
    "sub_post_view": ["throat_be", "throat_py"],
    "sub_post_vote": ["throat_be", "throat_py"],
    "sub_rule": ["throat_be", "throat_py"],
    "sub_stylesheet": ["throat_be", "throat_py"],
    "sub_subscriber": ["ovarit_auth", "throat_be", "throat_py"],
    "sub_uploads": ["throat_be", "throat_py"],
    "sub_user_flair": ["throat_be", "throat_py"],
    "sub_user_flair_choice": ["throat_be", "throat_py"],
    "user": ["ovarit_auth", "ovarit_subs", "throat_be", "throat_py"],
    "user_content_block": ["throat_be", "throat_py"],
    "user_message_block": ["throat_be", "throat_py"],
    "user_message_mailbox": ["throat_be", "throat_py"],
    "user_metadata": ["ovarit_auth", "ovarit_subs", "throat_be", "throat_py"],
    "user_saved": ["throat_be", "throat_py"],
    "user_unread_message": ["throat_be", "throat_py"],
    "user_uploads": ["throat_be", "throat_py"],
    "wiki": ["throat_be", "throat_py"],
}


def migrate(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql("REVOKE CREATE ON SCHEMA public FROM public;")
        for user in ["ovarit_auth", "ovarit_subs", "throat_be", "throat_py"]:
            migrator.sql(
                f'GRANT USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public TO "{user}";'
            )
            migrator.sql(
                f'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT USAGE, SELECT ON SEQUENCES TO "{user}";'
            )

        for table, users in table_permissions.items():
            for user in users:
                migrator.sql(
                    f'GRANT SELECT, INSERT, UPDATE, DELETE ON TABLE "{table}" TO "{user}"'
                )


def rollback(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql("GRANT CREATE ON SCHEMA public TO public;")
        for user in ["ovarit_auth", "ovarit_subs", "throat_be", "throat_py"]:
            migrator.sql(
                f'REVOKE USAGE, SELECT ON ALL SEQUENCES IN SCHEMA public FROM "{user}";'
            )
            migrator.sql(
                f'ALTER DEFAULT PRIVILEGES IN SCHEMA public REVOKE USAGE, SELECT ON SEQUENCES FROM "{user}";'
            )
        for table, users in table_permissions.items():
            for user in users:
                migrator.sql(
                    f'REVOKE SELECT, INSERT, UPDATE, DELETE ON TABLE "{table}" FROM "{user}"'
                )
