"""Peewee migrations -- 048_delete_empty_user_flair.py

It was possible for users to create empty user flairs by entering whitespace
in the dialog.  Remove any empty flairs.
"""
import datetime as dt
import peewee as pw
from slugify import slugify

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    SubUserFlair = migrator.orm["sub_user_flair"]
    if not fake:
        SubUserFlair.delete().where(SubUserFlair.flair == "").execute()


def rollback(migrator, database, fake=False, **kwargs):
    pass
