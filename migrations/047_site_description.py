"""Peewee migrations -- 047_site_description

Add an admin site configuration options for a site description to
supply to search robots and link preview fetches.
"""

import peewee as pw

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    SiteMetadata = migrator.orm["site_metadata"]
    if not fake:
        SiteMetadata.create(
            key="site.description",
            value="Ovarit is a platform for women-centered communities featuring conversations "
            "on politics, news, media, life, activism, and gender-critical and radical feminism.",
        )


def rollback(migrator, database, fake=False, **kwargs):
    SiteMetadata = migrator.orm["site_metadata"]
    if not fake:
        SiteMetadata.delete().where(SiteMetadata.key == "site.description").execute()
