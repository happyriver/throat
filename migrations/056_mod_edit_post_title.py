"""Peewee migrations -- 056_mod_edit_post_title.py

Add a uid field to the SubPostTitleHistory table.  Initialize it
with the uid of the author of the post.

"""
import datetime as dt
from enum import IntEnum
import peewee as pw

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    SubPostTitleHistory = migrator.orm["sub_post_title_history"]
    User = migrator.orm["user"]
    migrator.add_fields(
        SubPostTitleHistory,
        uid=pw.ForeignKeyField(column_name="uid", model=User, field="uid", null=True),
    )
    if not fake:
        migrator.run()
        migrator.sql(
            """
        UPDATE sub_post_title_history
           SET uid = sub_post.uid
          FROM sub_post
         WHERE sub_post.pid = sub_post_title_history.pid
        """
        )

    migrator.add_not_null("sub_post_title_history", "uid")


def rollback(migrator, database, fake=False, **kwargs):
    SubPostTitleHistory = migrator.orm["sub_post_title_history"]
    migrator.remove_fields(SubPostTitleHistory, "uid")
