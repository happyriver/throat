"""Peewee migrations -- 044_fix_timestamps.py

Correct an error in the timestamp created by 039_best_comment_sort.py.
"""
import datetime as dt
import peewee as pw

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    if not fake:
        SiteMetadata = migrator.orm["site_metadata"]
        sm = SiteMetadata.get(SiteMetadata.key == "best_comment_sort_init")
        if sm.value.find("dT") != -1:
            d = dt.datetime.strptime(sm.value, "%Y-%m-%UdT%H:%M:%SZ")
            sm.value = d.strftime("%Y-%m-%dT%H:%M:%SZ")
            sm.save()


def rollback(migrator, database, fake=False, **kwargs):
    if not fake:
        SiteMetadata = migrator.orm["site_metadata"]
        sm = SiteMetadata.get(SiteMetadata.key == "best_comment_sort_init")
        if sm.value.find("dT") == -1:
            d = dt.datetime.strptime(sm.value, "%Y-%m-%dT%H:%M:%SZ")
            sm.value = d.strftime("%Y-%m-%UdT%H:%M:%SZ")
            sm.save()
