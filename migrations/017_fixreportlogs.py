"""Peewee migrations -- 017_fixreportlogs.py.
"""

import datetime as dt
import peewee as pw

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    """Write your migrations here."""

    @migrator.create_model
    class PostReportLog(pw.Model):
        rid = pw.ForeignKeyField(
            column_name="id", model=migrator.orm["sub_post_report"], field="id"
        )
        action = pw.IntegerField(null=True)
        desc = pw.CharField(null=True)
        lid = pw.AutoField()
        link = pw.CharField(null=True)
        time = pw.DateTimeField()
        uid = pw.ForeignKeyField(
            column_name="uid", null=True, model=migrator.orm["user"], field="uid"
        )
        target = pw.ForeignKeyField(
            column_name="target_uid", null=True, model=migrator.orm["user"], field="uid"
        )

        def __repr__(self):
            return f"<PostReportLog action={self.action}>"

        class Meta:
            table_name = "post_report_log"


def rollback(migrator, database, fake=False, **kwargs):
    """Write your rollback migrations here."""
    migrator.remove_model("PostReportLog")
