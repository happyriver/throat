""""Peewee migrations -- 054_deleted_user_logins.py

Remove the user_login records for all deleted users.

"""
import datetime as dt
from enum import IntEnum
import peewee as pw
import playhouse.postgres_ext as pw_ext

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.run()
        migrator.sql(
            """
        DELETE FROM user_login ul
         USING "user" u
         WHERE u.uid = ul.uid
           AND u.status = 10
        """
        )


def rollback(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql(
            """
        INSERT INTO user_login (uid, email, parsed_hash)
        SELECT u.uid, '', ''
          FROM "user" u
         WHERE u.status = 10
        """
        )
