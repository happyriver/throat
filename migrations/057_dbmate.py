"""Peewee migrations -- 057_dbmate.py

Say goodbye to Peewee, and hello to dbmate.  Record the initial dbmate
migration as having been run, so that we don't double-migrate.

"""
import datetime as dt
from enum import IntEnum
import peewee as pw

SQL = pw.SQL


def migrate(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql(
            """
        CREATE TABLE public.schema_migrations (
            version text NOT NULL
        );
        """
        )
        migrator.sql(
            """
        INSERT INTO public.schema_migrations (version) VALUES ('20230613191133');
        """
        )


def rollback(migrator, database, fake=False, **kwargs):
    if not fake:
        migrator.sql(
            """
        DROP TABLE IF EXISTS public.schema_migrations;
        """
        )
