# Ovarit's fork of Throat

A link and discussion aggregator  forked from https://github.com/phuks-co/throat.

## Development

See https://gitlab.com/happyriver/fc-devel for a turnkey development
environment based on Docker.

## Production deployments
 
See [doc/deploy.md](doc/deploy.md) for instructions to deploy on
`gunicorn` or using Docker.

## Deploying to AWS

You can check out the [CDK Definition of
Infrastructure](https://gitlab.com/feminist-conspiracy/infrastructure)
maintained by Ovarit.

## Support

Reach out to the admins of Ovarit using the contact information here:
https://ovarit.com/wiki/about.
